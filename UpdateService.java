package com.globus_ltd.leagueoflight.services;

import android.app.IntentService;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.globus_ltd.leagueoflight.LeagueApplication;
import com.globus_ltd.leagueoflight.R;
import com.globus_ltd.leagueoflight.data.database.DatabaseHelper;
import com.globus_ltd.leagueoflight.data.database.models.Luster;
import com.globus_ltd.leagueoflight.data.database.models.LusterUpdate;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class UpdateService extends IntentService {
    private static final String TAG = UpdateService.class.getSimpleName();
    private static final int NOTIFICATION_ID = 100;
    private static final String UPDATES_FILENAME = "updates.zip";
    private static final String UPDATES_DATABASE_FILENAME = "updates.db";
    private static final String IMAGES_DIRECTORY = "images/";
    private static final byte[] BUFFER = new byte[1024];

    public static Intent getIntent(@NonNull final Context context) {
        return new Intent(context, UpdateService.class);
    }

    public UpdateService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@NonNull final Intent intent) {
        startForeground(NOTIFICATION_ID, createNotification());
        try {
            Log.v(TAG, "Update...");
            final File updatesDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
            final File updatesFile = new File(updatesDirectory, UPDATES_FILENAME);
            if (updatesFile.exists()) {
                Log.v(TAG, "Updates file finded: " + updatesFile);
                final ZipFile zipFile = new ZipFile(updatesFile);
                final File databaseFile = extractDatabaseTempFile(zipFile);
                synchronizeDatabases(databaseFile);
                databaseFile.delete();
                extractUpdatedPhotos(zipFile);
                zipFile.close();
                updatesFile.delete();
            } else {
                Log.v(TAG, "Updates file not found.");
            }
            Log.v(TAG, "Update finished.");
        } catch (Throwable e) {
            Log.e(TAG, "Update error", e);
        }
        stopForeground(true);
    }

    @NonNull
    private Notification createNotification() {
        return new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.update_title))
                .setContentText(getString(R.string.update_description))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setProgress(0, 0, true)
                .build();
    }

    private void synchronizeDatabases(@NonNull final File databaseFile) {
        Log.v(TAG, "Synchronize database with " + databaseFile);
        getDatabaseHelper().attachUpdatesDatabase(databaseFile.getPath());
        synchronizeDeleted();
        synchronizeUpdated();
        getDatabaseHelper().detachUpdatesDatabase();
    }

    private void synchronizeDeleted() {
        Log.v(TAG, "Delete lusters");
        final DatabaseHelper dbHelper = getDatabaseHelper();
        final Cursor cursor = dbHelper.getRemovedLusters();
        Log.v(TAG, "Count: " + cursor.getCount());
        if (cursor.getCount() > 0) {
            final Luster luster = new Luster();
            cursor.moveToFirst();
            do {
                luster.setValues(cursor);
                if (luster.hasPhoto1()) {
                    removePhoto(luster.getPhoto1());
                }
                if (luster.hasPhoto2()) {
                    removePhoto(luster.getPhoto2());
                }
                dbHelper.deleteLuster(luster.getArticle());
                Log.v(TAG, "Removed " + luster);
                luster.resetDefaults();
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private void synchronizeUpdated() {
        Log.v(TAG, "Update lusters");
        final DatabaseHelper dbHelper = getDatabaseHelper();
        final Cursor cursor = dbHelper.getAllLusterUpdates();
        Log.v(TAG, "Count: " + cursor.getCount());
        if (cursor.getCount() > 0) {
            final Luster luster = new Luster();
            final LusterUpdate lusterUpdate = new LusterUpdate();
            cursor.moveToFirst();
            do {
                lusterUpdate.setValues(cursor);
                if (dbHelper.getLuster(lusterUpdate.getArticle(), luster) == null) {
                    luster.updateValues(lusterUpdate);
                    dbHelper.insertOrUpdateLuster(luster);
                    Log.v(TAG, "Added luster " + luster);
                } else {
                    if (luster.shouldUpdate(lusterUpdate)) {
                        if (luster.shouldRemovePhoto1(lusterUpdate)) {
                            removePhoto(luster.getPhoto1());
                        }
                        if (luster.shouldRemovePhoto2(lusterUpdate)) {
                            removePhoto(luster.getPhoto2());
                        }
                        luster.updateValues(lusterUpdate);
                        dbHelper.insertOrUpdateLuster(luster);
                        Log.v(TAG, "Update luster " + luster);
                    }
                }
                luster.resetDefaults();
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private void extractUpdatedPhotos(@NonNull final ZipFile zipFile) {
        final DatabaseHelper dbHelper = getDatabaseHelper();
        final Cursor cursor = dbHelper.getLustersForUpdatePhotos();
        Log.v(TAG, "Lusters with not updated photos count: " + cursor.getCount());
        if (cursor.getCount() > 0) {
            final Luster luster = new Luster();
            cursor.moveToFirst();
            do {
                luster.setValues(cursor);
                if (!luster.isPhoto1Loaded()) {
                    final boolean extracted = extractPhoto(zipFile, luster.getPhoto1());
                    luster.setPhoto1Loaded(extracted);
                }
                if (!luster.isPhoto2Loaded()) {
                    final boolean extracted = extractPhoto(zipFile, luster.getPhoto2());
                    luster.setPhoto2Loaded(extracted);
                }
                dbHelper.insertOrUpdateLuster(luster);
                Log.v(TAG, "Photos extracted " + luster);
                luster.resetDefaults();
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private DatabaseHelper getDatabaseHelper() {
        return ((LeagueApplication) getApplication()).getDatabaseHelper();
    }

    private void removePhoto(@NonNull final String path) {
        final File photoFile = new File(getExternalFilesDir(null), IMAGES_DIRECTORY + path);
        final boolean deleted = photoFile.delete();
        Log.v(TAG, "Remove photo " + photoFile + " : " + deleted);
    }

    private boolean extractPhoto(@NonNull final ZipFile zipFile, @NonNull final String path) {
        Log.v(TAG, "Extract photo " + path);
        try {
            final ZipEntry entry = zipFile.getEntry(IMAGES_DIRECTORY + path);
            final InputStream inputStream = zipFile.getInputStream(entry);
            final File photoFile = new File(getExternalFilesDir(null), IMAGES_DIRECTORY + path);
            photoFile.delete();
            photoFile.getParentFile().mkdirs();
            writeFile(photoFile, inputStream);
            inputStream.close();
            return true;
        } catch (Throwable e) {
            Log.e(TAG, "Extract photo error", e);
        }
        return false;
    }

    @NonNull
    private File extractDatabaseTempFile(@NonNull final ZipFile zipFile) throws IOException {
        final File databaseFile = new File(getFilesDir(), UPDATES_DATABASE_FILENAME);
        databaseFile.delete();
        final InputStream inputStream = zipFile.getInputStream(zipFile.getEntry(UPDATES_DATABASE_FILENAME));
        writeFile(databaseFile, inputStream);
        inputStream.close();
        return databaseFile;
    }

    private void writeFile(@NonNull final File outputFile, @NonNull final InputStream inputStream) throws IOException {
        final OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));
        int length;
        while ((length = inputStream.read(BUFFER)) != -1) {
            outputStream.write(BUFFER, 0, length);
        }
        outputStream.close();
    }

}
